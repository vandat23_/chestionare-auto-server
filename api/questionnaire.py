
import cherrypy

from api.response import Response
from dataParser.getquestionnaire import Questionnaire as Q
from dataParser.dbconnection import DataBaseConnection as DB

class Questionnaire():
    exposed = True

    @cherrypy.tools.json_out()
    def GET(self, id):
        q = Q()
        if id == 10 or id == 11:
            id = 12
        res = Response('success', q.getQuestionnaire(id))
        self.id = id
        return res.to_json()

    @cherrypy.tools.json_out()
    def POST(self,username,score,questionnaireId):
        dbObject = DB()
        dbObject.openConnection()
        result = dbObject.insertAnswers(username,score,questionnaireId)
        dbObject.closeConnection()
        if result == True :
            res = Response('success', {})
        else:
            res = Response('failed', {})

        return res.to_json()