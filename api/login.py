from dbConnections.authentication import Authentication
import cherrypy

class Login:
    # expose all the functions from this class
    exposed = True

    @cherrypy.tools.json_out()
    def POST(self, username, password):
        auth = Authentication()
        return auth.login(username, password)
		