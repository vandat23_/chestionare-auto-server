class Response:
    status = 'success'
    data = []

    def __init__(self, status, data):
        self.status = status
        self.data = data

    def to_json(self):
        return {
            'status': self.status,
            'data': self.data
        }
