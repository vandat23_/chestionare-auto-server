import cherrypy
from api.response import Response
from dbConnections.stats import StatsDB


class Stats():
    exposed = True

    @cherrypy.tools.json_out()
    def GET(self, email):
        s = StatsDB()
        data = s.getStats(email)
        #res = Response('success', data)
        #print(res.to_json())
        return data
