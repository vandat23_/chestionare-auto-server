__author__ = 'vanda'

import sqlite3
import datetime
from dataParser.qparser import Parser
from api.response import Response
import datetime


class DataBaseConnection:


    def __init__(self):

        self.answer_id = 1
        self.question_id = 0
        self.raspunsuri = list()
        self.intrebari = list()

        self.db_name = 'db.sqlite3'
        self.questions_table_name = "questions"
        self.answers_table_name = "answers"
        self.users_table_name = "users"
        self.user_answers_table_name = "user_answers"
        self.users_table_username_field = "email"
        self.users_table_pass_field = "password"
        self.user_answers_userID_field = "user_id"
        self.user_answers_date_field = "date"
        self.user_answers_points_field = "points"
    
    
    def openConnection(self):

        #create connection to db
        self.conn = sqlite3.connect(self.db_name)
        self.conn.text_factory = str

        #create cursor for db
        self.c = self.conn.cursor()

    def closeConnection(self):
        self.conn.close()

    def insertInfoInTables(self):

        statement = 'INSERT INTO %s VALUES (?,?,?,?)' %self.questions_table_name
        self.c.executemany(statement, self.intrebari)
        statement = 'INSERT INTO %s VALUES (?,?,?,?)' %self.answers_table_name
        self.c.executemany(statement,self.raspunsuri)

        self.conn.commit()

        print('SQLLITE3 : inserted info into tables : questions / answers')


    def getQuestionnaire(self,noQuestionnaire):

        #status to return in final dict
        status = 'success'


        field = 'questionnaire_id'
        statement = 'SELECT * FROM %s WHERE %s=%s' %(self.questions_table_name,field,str(noQuestionnaire))
        self.c.execute(statement)

        list_intrebari = self.c.fetchall()
        list_id_intrebari = list()
        list_raspunsuri = list()
        list_id_intrebari = [i[0] for i in list_intrebari]

        # getting answers for questions

        field = 'question_id'
        for item in list_id_intrebari:
            statement = 'SELECT * FROM %s WHERE %s=%s' %(self.answers_table_name,field,str(item))
            self.c.execute(statement)
            temp = self.c.fetchall()
            list_raspunsuri.append(temp)



        print('SQLLITE3 : queried tables : questions / answers for %s' %str(noQuestionnaire))

        # construction of final dictionary
        dict_final = {}
        full_ques_list = list()

        for item in list_intrebari:
            dict_question = {}
            text = item[-2]
            poza = item[-1]
            id = item[1]
            answers_list = list()
            for answer in list_raspunsuri[list_intrebari.index(item)]:
                ques_id = answer[1]
                dict_answer = {}
                dict_answer['text'] = answer[2]
                dict_answer['is_correct'] = answer[3]
                answers_list.append(dict_answer)
                if not answers_list:
                    status = 'failure'
            dict_question['text'] = text
            dict_question['poza'] = poza
            dict_question['answers'] = answers_list
            full_ques_list.append(dict_question)



        if not full_ques_list:
            status = 'failure'
        if len(full_ques_list) < 26 :
            status = 'failure'

        dict_final['questions'] = full_ques_list
        dict_final['status'] = status
       # dict_final = {'chestionar' : full_ques_list, 'status' : status}
       # dict_final = (sorted(dict_final.items()))


        return dict_final

    def modelQuestion(self,question,index,chestionar):


        # textul intrebarii
        question_text = question[0]

        #setat id intrebare

        self.question_id = self.question_id + 1

        #setat id chestionar
        chestionar_id = chestionar

        # raspunsurile + raspunsurile corecte
        question_answers = question[1]

        # doar raspunsurile corecte
        string_right_answers = question_answers[1]

        #doar raspunsurile
        question_answers = question_answers[0]


        # vector de 0/1 pt raspunsuri
        is_right = [0]*4
        list_right_answers = string_right_answers.split()
        for item in list_right_answers:
            is_right[ord(item) - 96] = 1

        temp_raspunsuri = list()

        #setat id intrebare pentru fiecare raspuns
        for item in question_answers:
            temp_raspunsuri.append(self.answer_id)
            self.answer_id = self.answer_id + 1
            temp_raspunsuri.append(self.question_id)
            temp_raspunsuri.append(item)
            temp_raspunsuri.append(is_right[question_answers.index(item)])
            temp_raspunsuri = tuple(temp_raspunsuri)
            self.raspunsuri.append(temp_raspunsuri)
            temp_raspunsuri = list()


        #url-ul imaginii, daca exista, daca nu va fi un string none
        question_image = question[2]

        temp_intrebare = list()
        temp_intrebare.append(self.question_id)
        temp_intrebare.append(chestionar_id)
        temp_intrebare.append(question_text)
        temp_intrebare.append(question_image)
        temp_intrebare = tuple(temp_intrebare)


        self.intrebari.append(temp_intrebare)




    def insertQuestions(self):

        parserObj = Parser()
        list_questions = parserObj.getAllQuestions()

        for list in list_questions :
            for item in list:
                self.modelQuestion(item,list.index(item)+1,list_questions.index(list)+1)

        self.insertInfoInTables()
    
    
    def checkUserExistence(self, username, password):
        statement = 'SELECT * FROM %s WHERE %s=\"%s\" AND %s=\"%s\"' %(self.users_table_name, self.users_table_username_field, str(username), self.users_table_pass_field, str(password))
        self.c.execute(statement)
        user = self.c.fetchone()
        
        if user is None:
            return False
        
        return True
        
    
    def login(self, username, password):
        # checking if the user exists
        response = None
        
        if self.checkUserExistence(username, password) == False:
            response = Response('failed', 'The user does not exist!')
        else:
            response = Response('success', 'Correct credentials!')
        
        return response.to_json()
        
    
    def insertUser(self, username, password):
        statement = 'INSERT INTO %s (%s, %s) VALUES (\"%s\", \"%s\");' %(self.users_table_name, self.users_table_username_field, self.users_table_pass_field, username, password)
        print(statement)
        try:
            self.c.execute(statement)
            self.conn.commit()
        
        except Exception as ex:
            print (ex)
            return False
        
        return True
    
    
    def register(self, username, password):
        # checking if the user exists
        response = None
        
        if self.checkUserExistence(username, password) == True:
            response = Response('Failed', 'The user already exists!')
        else:
            if self.insertUser(username, password) == True:
                response = Response('success', 'Added user ' + username + '.')
            else:
                response = Response('failed', 'Encountered an error while inserting into the database!')
        
        return response.to_json()
    
    
    def insertAnswers(self,username,score,questionnaireId):

         now = datetime.datetime.today()
         now = str(now.strftime("%Y-%m-%d %H:%M:%S"))

         statement = 'SELECT id FROM %s WHERE %s=\"%s\"' %(self.users_table_name,self.users_table_username_field,username)

         try :
             self.c.execute(statement)
             user_id = self.c.fetchone()
         except Exception as ex:
             print(ex)
             return False
         
         if user_id is None:
            return False
         
         statement = 'INSERT INTO %s VALUES (\"%s\", \"%s\",\"%s\",\"%s\")' %(self.user_answers_table_name,str(user_id[0]),str(questionnaireId),str(now),str(score))
         print(statement)
         
         try:
             self.c.execute(statement)
             self.conn.commit()
         
         except Exception as ex:
             print(ex)
             return False

         return True
    
    
    def getStats(self, email):
        response_data = {}
        
        statement = 'SELECT * FROM %s WHERE %s = \"%s\";' %(self.users_table_name, self.users_table_username_field, email)
        #print(statement)
        self.c.execute(statement)
        data = self.c.fetchone()
        
        if data is None:
            res = Response('failed', {})
            return res.to_json()
        
        userID = data[0]
        
        # azi
        now = datetime.datetime.today()
        today = str(now.strftime("%Y-%m-%d"))
        
        statement = 'SELECT %s FROM %s WHERE %s = %s AND %s like \"%%%s%%\" ORDER BY %s asc;' %(self.user_answers_points_field, self.user_answers_table_name, self.user_answers_userID_field, userID, self.user_answers_date_field, today, self.user_answers_date_field)
        
        #print(statement)
        
        self.c.execute(statement)
        rows = self.c.fetchall()
        
        data2 = []
        for row in rows:
            data2.append( row[0] )
        
        response_data["today"] = data2
        
        
        # ultimele 7 zile
        lastWeekTemp = now - datetime.timedelta(days=7)
        lastWeek = str(lastWeekTemp.strftime("%Y-%m-%d"))
        
        statement = 'SELECT %s, %s FROM %s WHERE %s = %s AND %s > \"%s\" ORDER BY %s desc;' %(self.user_answers_date_field, self.user_answers_points_field, self.user_answers_table_name, self.user_answers_userID_field, userID, self.user_answers_date_field, lastWeek, self.user_answers_date_field)
        
        print(statement)
        self.c.execute(statement)
        rows = self.c.fetchall()
        print (rows)
        
        data2 = []
        dayIndex = lastWeekTemp.replace(hour=0, minute=0, second=0)
        
        while dayIndex <= now:
            qPassed = 0
            qTotal = 0
            dayWithoutTime = datetime.datetime(dayIndex.year, dayIndex.month, dayIndex.day)
            
            for row in rows:
                if row[0].find(dayIndex.strftime("%Y-%m-%d")) != -1:
                    qTotal = qTotal + 1
                    if row[1] >= 22:
                        qPassed = qPassed + 1
            
            data2.append({
                'date': dayIndex.strftime("%Y-%m-%d"),
                'passed': qPassed,
                'total': qTotal
            })
            dayIndex = dayIndex + datetime.timedelta(days=1)
        
        
        response_data['lastWeek'] = data2
        
        
        # toata istoria
        statement = 'SELECT COUNT(*) FROM %s WHERE %s = %s;' %(self.user_answers_table_name, self.user_answers_userID_field, userID)
        
        self.c.execute(statement)
        row = self.c.fetchone()
        
        # numar total de chestionare
        global_total = row[0]
        
        
        statement = 'SELECT COUNT(*) FROM %s WHERE %s = %s AND %s >= 22;' %(self.user_answers_table_name, self.user_answers_userID_field, userID, self.user_answers_points_field)
        
        self.c.execute(statement)
        row = self.c.fetchone()
        
        # numar de chestionare "trecute"
        global_passed = row[0]
        
        response_data['global'] = { 
            'passed': global_passed, 
            'total': global_total 
            }
        
        res = Response('success', response_data)
        return res.to_json()
    
    