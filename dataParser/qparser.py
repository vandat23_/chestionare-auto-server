__author__ = 'vanda'

import urllib
import urllib.request
import urllib.parse
import re

class Parser:

    def __init__(self):
        self.urlChestionar = "http://chestionare.auto.ro/chestionar-auto-generat/chestionar-1"


    def getAllQuestions(self):

        print ("QuestionParser : getting questions")
        all_questions = list()
        chestionar = 1
        while chestionar <= 30 :
            self.urlChestionar = "http://chestionare.auto.ro/chestionar-auto-generat/chestionar-" + str(chestionar)
            print ("QuestionParser : getting questions for questionnaire " + str(chestionar))
            temp_list = self.getQuestions()
            chestionar = chestionar + 1
            all_questions.append(temp_list)
        return all_questions



    def getQuestions(self):


        #place POST data in a dictionary
        post_data_dictionary = {}

        #encode the POST data to be sent in a URL
        post_data_encoded = urllib.parse.urlencode(post_data_dictionary)
        binary_data = post_data_encoded.encode('utf-8')

        #make a request object to hold the POST data and the URL
        request_object = urllib.request.Request(self.urlChestionar, binary_data)

        #make the request using the request object as an argument, store response in a variable
        response = urllib.request.urlopen(request_object)

        #store request response in a string
        website_text = str(response.read().decode('utf8'))


        #declaration of lists used
        questions = list()
        match_set = list()
        values = list()
        answers_list = list()
        aux_list = list()
        question_with_images = list()
        final_list = list()


        images = re.findall('<img .*src="/chestionare(.*?)"', website_text)
        question_matches = re.findall('(<h2>.*?[\\?|:])+',website_text)
        answer_matches = re.findall('(<label>.*?</label>)+',website_text,re.DOTALL)
        temp = re.findall('(<li class="cu_poza">.*?<h2>\d+)+',website_text,re.DOTALL)
        right_answers = re.findall('(Varianta corecta.*?[abcd].*[abcd].*[abcd].*[abcd])+',website_text)

        for index,item in enumerate(right_answers):
            right_answers[index] = right_answers[index].replace(")","")
            right_answers[index] = right_answers[index].replace("Varianta corecta:","")
            right_answers[index] = right_answers[index].replace("Variante bifa","")
            #right_answers[index] = right_answers[index].split(":")[0]

        for item in temp:
            aux = re.findall('>\d+',item)
            if not question_with_images:
               question_with_images = aux
            else:
               question_with_images.extend(aux)

        question_with_images = [item.replace('>','') for item in question_with_images]


        for question in question_matches[:] :
                question = question.replace("<h2>"," ")
                question = question.replace("</h2>"," ")
                questions.append(question)


        for image in images:
                match_set.append("http://chestionare.auto.ro/chestionare" + image)

        answers_list = [answer_matches[n:n+3] for n in range(0, len(answer_matches), 3)]


        for item in answers_list :
            for id in item:
                answer = id.replace("<label>","")
                answer = answer.replace("</label>","")
                aux_list.append(answer)
            values.append(aux_list)
            aux_list = list()




        extra = answer_matches[len(answer_matches)-1].replace("<label>","")
        extra = extra.replace("</label>","")
        values.append(extra)

        values = list(zip(values,right_answers))

        question_answers_dictionary = (zip(questions,values))
        question_image_dictionary = dict(zip(question_with_images,match_set))

        for key,value in question_answers_dictionary:

            aux_list = list()

            question_number = re.search('\d+',key)
            question_number = question_number.group(0)
            aux_list.append(key)
            aux_list.append(value)
            temp = question_image_dictionary.get(str(question_number))
            if not temp :
               aux_list.append("none")
            else :
               aux_list.append(temp)
            final_list.append(aux_list)
        return final_list