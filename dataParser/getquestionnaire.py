__author__ = 'vanda'

from dataParser.dbconnection import DataBaseConnection

class Questionnaire:


    def __init__(self):

        self.dbqinstance = DataBaseConnection()

    def getQuestionnaire(self,noQuestionnaire):

        self.dbqinstance.openConnection()
        q = self.dbqinstance.getQuestionnaire(noQuestionnaire)
        self.dbqinstance.closeConnection()
        return q

# exemplu de apelare si ce intoarce : https://docs.google.com/document/d/19DtXlGFg8J--kvtLxVEr8wc-Lcbhwo1n2ROfWfvXUd4/edit?usp=sharing
#ques  = Questionnaire()
#dict = ques.getQuestionnaire(2)
#print(dict)
