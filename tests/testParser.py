from unittest import TestCase
from dataParser.qparser import Parser

__author__ = 'vanda'


class TestParser(TestCase):

    def setUp(self):
        self.urlChestionar = "http://chestionare.auto.ro/chestionar-auto-generat/chestionar-1"
        self.parserInstance = Parser()

    def test_getAllQuestion(self):
        testList = self.parserInstance.getAllQuestions()
        self.assertEquals(len(testList), (30), 'incorrect list of questions list size ' + str(len(testList)))

    def test_getQuestions(self):
        testList = self.parserInstance.getQuestions()
        for item in testList:
            self.assertEquals(len(item) ,(3),'incorrect question size '+ str(len(item)))
        self.assertEqual(len(testList), (26), 'incorrect questions list size '+ str(len(testList)))