
import unittest
from dbConnections.authentication import Authentication

class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.ainstance = Authentication()
        self.valid_params_login   = [{ 'username' : 'vanda@yahoo.com' , 'password' : 'parola' }
                                    ]
        self.invalid_params_login = [{ 'username' : 'newuse@yahoo.com' , 'password' : 'parola2' }
                                    # { 'username' : '' , 'password' : '' }
                                    ]
        self.valid_params_register   = [{ 'username' : 'newuser3@yahoo.com' , 'password' : 'parola' }]
        self.invalid_params_register = [{ 'username' : 'vanda@yahoo.com' , 'password' : 'parola2' }
                                       ]

    def test_login(self):

        for item in self.invalid_params_login:
            result = self.ainstance.login(item['username'],item['password'])
            print(result['data'])
            print(result['status'])
            self.assertEqual(result['status'], 'failed')


        for item in self.valid_params_login:
            result = self.ainstance.login(item['username'],item['password'])
            print(result['data'])
            print(result['status'])
            self.assertEqual(result['status'], 'success')

    def test_register(self):


        for item in self.invalid_params_register:
            result = self.ainstance.register(item['username'],item['password'])
            print(result['data'])
            print(result['status'])
            self.assertEqual(result['status'], 'Failed')

        for item in self.valid_params_register:
            result = self.ainstance.register(item['username'],item['password'])
            print(result['data'])
            print(result['status'])
            self.assertEqual(result['status'], 'success')


if __name__ == '__main__':
    unittest.main()