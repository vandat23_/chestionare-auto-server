
import unittest
from dataParser.dbconnection import DataBaseConnection

class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.dbqinstance = DataBaseConnection()
        self.dbqinstance.openConnection()
        self.noQues = 1
        self.valid_user_credentials   = [ {'username' : 'example@yahoo.com' , 'password' : 'parola' }
                                        ]
        self.invalid_user_credentials = [ {'username' : 'nosuchuser@yahoo.com' , 'password' : 'parola' },
                                          {'username' : '' , 'password' : '' }
                                        ]

        self.valid_save_answers       = [ {'username' :'example@yahoo.com' ,'score':'20' ,'questionnaireId': '3'},
                                          {'username' :'example@yahoo.com' ,'score':'23' ,'questionnaireId': '25'}
                                        ]
        self.invalid_save_answers     = [ {'username' :'invalidusername@yahoo.com' ,'score':'20' ,'questionnaireId': '1'},
                                        ]

    def test_getOneQuestionnaire(self):

        test_dict = self.dbqinstance.getQuestionnaire(self.noQues)

        if self.noQues > 0 and self.noQues < 31 :
            self.assertEqual(test_dict['status'], 'success')
        else :
            self.assertEqual(test_dict['status'], 'failure')

    def test_checkUserExistence(self):

        for item in self.valid_user_credentials:
            result = self.dbqinstance.checkUserExistence(item['username'],item['password'])
            self.assertEquals(result,True)

        for item in self.invalid_user_credentials:
            result = self.dbqinstance.checkUserExistence(item['username'],item['password'])
            self.assertEquals(result,False)

    def test_saveAnswers(self):

        for item in self.valid_save_answers:
            result = self.dbqinstance.insertAnswers(item['username'],item['score'],item['questionnaireId'])
            self.assertEquals(result,True)
        for item in self.invalid_save_answers:
            print(item)
            result = self.dbqinstance.insertAnswers(item['username'],item['score'],item['questionnaireId'])
            self.assertEquals(result,False)


if __name__ == '__main__':
    unittest.main()
