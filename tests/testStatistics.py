import unittest
import sys
sys.path.append('D:\Master\EPA\chestionare-auto-server')

from dbConnections.stats import StatsDB


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.ainstance = StatsDB()
        self.valid_params   = [{ 'username' : 'andreea@yahoo.com' }]
        self.invalid_params = [{ 'username' : 'someuser@yahoo.com'}]
    
    
    def testGetStats(self):
        for item in self.invalid_params:
            result = self.ainstance.getStats(item['username'])
            print(result['data'])
            print(result['status'])
            self.assertEqual(result['status'], 'failed')
        
        for item in self.valid_params:
            result = self.ainstance.getStats(item['username'])
            print(result['data'])
            print(result['status'])
            self.assertEqual(result['status'], 'success')


if __name__ == '__main__':
    unittest.main()
    
    