CREATE TABLE users
(
  id       INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
  email    VARCHAR(256) NOT NULL,
  password VARCHAR(32)  NOT NULL
);

CREATE TABLE questions
(
  id               INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  questionnaire_id INTEGER NOT NULL,
  text             TEXT    NOT NULL,
  image            VARCHAR(256)
);


CREATE TABLE answers
(
  id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  question_id INTEGER NOT NULL,
  text        TEXT    NOT NULL,
  is_correct  BOOLEAN NOT NULL,
  FOREIGN KEY (question_id) REFERENCES questions (id)
);


CREATE TABLE user_answers
(
  user_id          INTEGER NOT NULL,
  questionnaire_id INTEGER NOT NULL,
  date             TEXT    NOT NULL,
  points           INTEGER NOT NULL,
  FOREIGN KEY (questionnaire_id) REFERENCES questions (questionnaire_id),
  FOREIGN KEY (user_id) REFERENCES users (id)
);

