import cherrypy

from api.login import Login
from api.questionnaire import Questionnaire
from api.register import Register
from api.stats import Stats


cherrypy.config.update({
    'server.socket_host': '0.0.0.0',
    'server.socket_port': 8080,
})

cherrypy.tree.mount(Login(), '/login', {
    '/': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher()
    }
})

cherrypy.tree.mount(Register(), '/register', {
    '/': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher()
    }
})

cherrypy.tree.mount(Stats(), '/stats', {
    '/': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher()
    }
})

cherrypy.tree.mount(Questionnaire(), '/questionnaire', {
    '/': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher()
    }
})

cherrypy.engine.start()
cherrypy.engine.block()