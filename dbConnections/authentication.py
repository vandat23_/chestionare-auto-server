from dataParser.dbconnection import DataBaseConnection

class Authentication:

    def __init__(self):
        self.dbqinstance = DataBaseConnection()
    
    def register(self, username, password):
        self.dbqinstance.openConnection()
        q = self.dbqinstance.register(username, password)
        self.dbqinstance.closeConnection()
        return q
    
    def login(self, username, password):
        self.dbqinstance.openConnection()
        q = self.dbqinstance.login(username, password)
        self.dbqinstance.closeConnection()
        return q
    