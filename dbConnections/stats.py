from dataParser.dbconnection import DataBaseConnection

class StatsDB:

    def __init__(self):
        self.dbqinstance = DataBaseConnection()
    
    def getStats(self, username):
        self.dbqinstance.openConnection()
        st = self.dbqinstance.getStats(username)
        self.dbqinstance.closeConnection()
        return st
    